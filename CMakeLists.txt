cmake_minimum_required(VERSION 3.24)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(test-tasks VERSION 0.0.0 LANGUAGES CXX)

set(QT_MIN_VERSION "6.7.2")

find_package(Qt6 ${QT_MIN_VERSION} REQUIRED COMPONENTS Widgets Core Gui Qml Test)

qt_standard_project_setup()

add_subdirectory(anagram_derivation)
add_subdirectory(dining_philosophers)
add_subdirectory(noughts_and_crosses)
add_subdirectory(tests)


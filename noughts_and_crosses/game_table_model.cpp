#include "game_table_model.h"

#include <QPoint>
#include <QDebug>

const int MIN_DIMENSION = 3;

GameTableModel::GameTableModel(QObject *parent)
    : QAbstractTableModel(parent)
    , m_dimension(0)
    , m_gameState(None)
{
    m_roleNames[GameTableModel::CellValueRole] = "cellValue";
}

bool GameTableModel::checkIndex(const QModelIndex &index) const
{
    return index.isValid() && index.row() < rowCount() && index.column() < columnCount();
}

int GameTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_dimension;
}

int GameTableModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_dimension;
}

QVariant GameTableModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index))
        return QVariant();

    if (role == Qt::EditRole)
        role = Qt::DisplayRole;

    if (role == Qt::DisplayRole || role == GameTableModel::CellValueRole)
        return m_matrix.at(index.row()).at(index.column());

    return QVariant();
}

bool GameTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!checkIndex(index))
        return false;

    if (role == Qt::DisplayRole)
        role = Qt::EditRole;

    if (role == Qt::EditRole || role == GameTableModel::CellValueRole) {
        m_matrix[index.row()][index.column()] = value.toInt();
        emit dataChanged(index, index);
        return true;
    }

    return false;
}

void GameTableModel::startGame(int newDimension)
{
    if (m_dimension != newDimension && newDimension >= MIN_DIMENSION) {
        m_dimension = newDimension;
        emit dimensionChanged(m_dimension);
    }

    beginResetModel();
    m_matrix.fill(QVector<int>(m_dimension, GameTableModel::None), m_dimension);
    endResetModel();

    switchState();
}

void GameTableModel::nextMove(const QModelIndex &index)
{
    if (gameState() == GameTableModel::None)
        return;

    int currentValue = index.data().toInt();
    if (currentValue != GameTableModel::None)
        return;

    if (setData(index, m_gameState)) {
        if (checkGameOver(index.row(), index.column()))
            return;
        switchState();
    }
}

bool GameTableModel::checkGameOver(int row, int column)
{
    if (gameState() == GameTableModel::None)
        return false;

    const int lastIndex = dimension() - 1;
    bool isDeadHeat = true;
    bool winLeftDiagonal = (row == column);
    bool winRightDiagonal = (lastIndex == (row + column));
    bool winRow = true;
    bool winColumn = true;
    for (int i = 0; i < dimension(); ++i) {
        const QVector<int> &values = m_matrix.at(i);
        for (int j = 0; j < dimension(); ++j) {
            int value = values.at(j);
            if (value != gameState()) {
                if (value == GameTableModel::None)
                    isDeadHeat = false;
                if (i == j)
                    winLeftDiagonal = false;
                if (lastIndex == (i + j))
                    winRightDiagonal = false;
                if (i == row)
                    winRow = false;
                if (j == column)
                    winColumn = false;
            }
            if (!isDeadHeat && !winLeftDiagonal && !winRightDiagonal && !winRow && !winColumn)
                return false;
        }
    }

    QVariantList points;
    if (winLeftDiagonal) {
        for (int i = 0; i < dimension(); ++i)
            points << QPoint(i, i);
    } else if (winRightDiagonal) {
        for (int i = 0; i < dimension(); ++i)
            points << QPoint(i, lastIndex - i);
    } else if (winRow) {
        for (int i = 0; i < dimension(); ++i)
            points << QPoint(row, i);
    } else if (winColumn) {
        for (int i = 0; i < dimension(); ++i)
            points << QPoint(i, column);
    }/* else if (isDeadHeat) {

    }*/

    emit gameOver(points);

    m_gameState = GameTableModel::None;
    emit gameStateChanged(m_gameState);

    return true;
}

void GameTableModel::switchState()
{
    if (m_gameState == GameTableModel::None || m_gameState == GameTableModel::Nought)
        m_gameState = GameTableModel::Cross;
    else
        m_gameState = GameTableModel::Nought;

    emit gameStateChanged(m_gameState);
}

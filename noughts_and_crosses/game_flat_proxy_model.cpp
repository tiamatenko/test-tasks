#include "game_flat_proxy_model.h"

#include <QPoint>

GameFlatProxyModel::GameFlatProxyModel(QObject *parent)
    : QAbstractProxyModel(parent)
{
    connect(this, &QAbstractProxyModel::sourceModelChanged, [&] {
        connect(sourceModel(), &QAbstractItemModel::modelReset, this, &GameFlatProxyModel::handleSourceModelReset);
        connect(sourceModel(), &QAbstractItemModel::dataChanged, this, &GameFlatProxyModel::handleSourceDataChanged);
    });
}

QModelIndex GameFlatProxyModel::index(int row, int column, const QModelIndex &parent) const
{
    return hasIndex(row, column, parent) ? createIndex(row, column) : QModelIndex();
}


QModelIndex GameFlatProxyModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child)
    return QModelIndex();
}

int GameFlatProxyModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return sourceModel()->rowCount() * sourceModel()->columnCount();
}

int GameFlatProxyModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 1;
}

QModelIndex GameFlatProxyModel::mapToSource(const QModelIndex &proxyIndex) const
{
    if (!proxyIndex.isValid())
        return QModelIndex();

    return sourceModel()->index(proxyIndex.row() / sourceModel()->columnCount(), proxyIndex.row() % sourceModel()->columnCount());
}

QModelIndex GameFlatProxyModel::mapFromSource(const QModelIndex &sourceIndex) const
{
    if (!sourceIndex.isValid())
        return QModelIndex();

    return index(sourceIndex.row() * sourceModel()->columnCount() + sourceIndex.column(), 0);
}

void GameFlatProxyModel::nextMove(int row)
{
    QModelIndex proxyIndex = index(row);
    QModelIndex sourceIndex = mapToSource(proxyIndex);
    if (sourceIndex.isValid())
        emit nextMoveDone(sourceIndex);
}

void GameFlatProxyModel::handleGameOver(const QVariantList &points)
{
    QVariantList indexes;
    for (const QVariant &v : points) {
        QPoint p = v.toPoint();
        QModelIndex sourceIndex = sourceModel()->index(p.x(), p.y());
        QModelIndex proxyIndex = mapFromSource(sourceIndex);
        indexes << proxyIndex.row();
    }

    emit gameOver(indexes);
}

void GameFlatProxyModel::handleSourceModelReset()
{
    beginResetModel();
    endResetModel();
}

void GameFlatProxyModel::handleSourceDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles)
{
    QModelIndex firstIndex = mapFromSource(topLeft);
    QModelIndex lastIndex = (topLeft == bottomRight ? firstIndex : mapFromSource(bottomRight));

    emit dataChanged(firstIndex, lastIndex, roles);
}

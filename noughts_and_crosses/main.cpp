#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include "game_table_model.h"
#include "game_flat_proxy_model.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setApplicationVersion(QStringLiteral("1.0"));
    app.setApplicationName(QStringLiteral("Noughts and Crosses"));

    qmlRegisterType<GameTableModel>("com.game.qmlcomponents", 1, 0, "GameModel");

    QQmlApplicationEngine engine;
    GameTableModel *gameModel = new GameTableModel(&app);
    GameFlatProxyModel *proxyModel = new GameFlatProxyModel(gameModel);
    proxyModel->setSourceModel(gameModel);
    QObject::connect(gameModel, &GameTableModel::gameOver, proxyModel, &GameFlatProxyModel::handleGameOver);
    QObject::connect(proxyModel, &GameFlatProxyModel::nextMoveDone, gameModel, &GameTableModel::nextMove);

    engine.rootContext()->setContextProperty(QStringLiteral("game_model"), gameModel);
    engine.rootContext()->setContextProperty(QStringLiteral("game_view_model"), proxyModel);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

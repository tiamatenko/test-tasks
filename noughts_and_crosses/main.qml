import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import com.game.qmlcomponents 1.0

Window {
    visible: true
    width: 840
    height: 680
    title: qsTr("Noughts and Crosses")
    color: 'yellow'

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        GridView {
            id: grid
            Layout.alignment: Qt.AlignCenter
            Layout.fillHeight: true
            Layout.preferredWidth: height

            property int spacing: 5

            cellWidth: Math.min(width, height) / game_model.dimension
            cellHeight: cellWidth
            interactive: false

            Connections {
                target: grid.contentItem
                onHeightChanged: canvas.redraw(canvas.firstIndex, canvas.lastIndex)
            }

            Component {
                id: cellDelegate
                Item {
                    width: grid.cellWidth
                    height: width

                    Flipable {
                        id: flipable
                        property bool flipped: cellValue >= 0

                        anchors.centerIn: parent
                        width: grid.cellWidth - grid.spacing*2
                        height: width
                        front: Rectangle {
                            anchors.fill: parent
                            color: 'green'
                        }
                        back: Image {
                            anchors.fill: parent
                            source: (cellValue > 0 ? "qrc:///resources/cross.png" : (!cellValue ? "qrc:///resources/nought.png" : ""))
                        }

                        transform: Rotation {
                            id: rotation
                            origin.x: flipable.width/2
                            origin.y: flipable.height/2
                            axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
                            angle: 0    // the default angle
                        }

                        states: State {
                            name: "back"
                            PropertyChanges { target: rotation; angle: 180 }
                            when: flipable.flipped
                        }

                        transitions: Transition {
                            NumberAnimation {
                                target: rotation
                                property: "angle"
                                duration: 1000
                                easing.type: Easing.OutBack
                            }
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: game_view_model.nextMove(index)
                        }
                    }
                }
            }

            model: game_view_model
            delegate: cellDelegate

            Canvas {
                id: canvas

                anchors.fill: parent

                property int firstIndex: -1
                property int lastIndex: -1
                property point firstPoint
                property point secondPoint

                renderTarget: Canvas.FramebufferObject
                renderStrategy: Canvas.Cooperative

                SequentialAnimation {
                    id: drawLineAnimation
                    PauseAnimation { duration: 1000 } // wait for end of image rotation animation
                    PropertyAnimation {
                        id: drawPointAnimation
                        target: canvas
                        property: "secondPoint"
                        duration: 1000
                        easing.type: Easing.OutQuint
                    }
                }

                function redraw(index1, index2) {
                    var repaint = (firstIndex !== index1 || lastIndex !== index2)
                    firstIndex = index1
                    lastIndex = index2

                    if (firstIndex < 0) {
                        if (repaint)
                            requestPaint()
                        return
                    }

                    console.log(firstIndex, lastIndex)
                    var i1 = grid.contentItem.children[firstIndex]
                    var i2 = grid.contentItem.children[lastIndex]
                    console.log(i1, i2)
                    var offset = grid.cellWidth / 20
                    var p1, p2
                    if (i1.x === i2.x) {
                        var x = i1.x + i1.width / 2
                        p1 = Qt.point(x, i1.y + offset)
                        p2 = Qt.point(x, i2.y + i2.height - offset)
                    }
                    else if (i1.y === i2.y) {
                        var y = i1.y + i1.height / 2
                        p1 = Qt.point(i1.x + offset, y)
                        p2 = Qt.point(i2.x + i2.width - offset, y)
                    }
                    else if (i1.x < i2.x) {
                        p1 = Qt.point(i1.x + offset, i1.y + offset)
                        p2 = Qt.point(i2.x + i2.width - offset, i2.y + i2.height - offset)
                    } else { // i1.x > i2.x
                        p1 = Qt.point(i1.x + i1.width - offset, i1.y + offset)
                        p2 = Qt.point(i2.x + offset, i2.y + i2.height - offset)
                    }

                    if (repaint) {
                        firstPoint = p1
                        drawPointAnimation.from = p1
                        drawPointAnimation.to = p2
                        drawLineAnimation.start()
                    } else {
                        firstPoint = p1
                        secondPoint = p2
                        requestPaint()
                    }
                }

                onPaint: {
                    var ctx = getContext("2d")
                    ctx.clearRect(0, 0, width, height)

                    if (firstIndex < 0)
                        return

                    ctx.strokeStyle = 'blue'
                    ctx.lineWidth = grid.cellWidth / 10
                    ctx.lineCap = "round"
                    ctx.beginPath()
                    ctx.moveTo(firstPoint.x, firstPoint.y)
                    ctx.lineTo(secondPoint.x, secondPoint.y)
                    ctx.stroke()
                }

                onSecondPointChanged: requestPaint()
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 50
            color: 'lightgray'
            RowLayout {
                anchors.fill: parent

                Text {
                    id: infoLabel
                    Layout.leftMargin: 10
                    Layout.preferredWidth: 200
                }

                Item { Layout.fillWidth: true }

                Text {
                    Layout.alignment: Qt.AlignVCenter
                    text: qsTr("Dimension:")
                }

                SpinBox {
                    id: dimensionSpinBox
                    Layout.alignment: Qt.AlignVCenter
                    minimumValue: 3
                    maximumValue: 10
                }

                Button {
                    Layout.alignment: Qt.AlignVCenter
                    Layout.rightMargin: 10
                    text: qsTr("Start Game")
                    onClicked: {
                        canvas.redraw(-1, -1)
                        game_model.startGame(dimensionSpinBox.value)
                    }
                }
            }

            Connections {
                target: game_model
                onGameStateChanged: {
                    switch (state) {
                    case GameModel.Nought:
                        infoLabel.text = qsTr("Nought move")
                        break
                    case GameModel.Cross:
                        infoLabel.text = qsTr("Cross move")
                    }
                }
            }

            Connections {
                target: game_view_model
                onGameOver: {
                    if (indexes.length > 0) {
                        infoLabel.text = qsTr("Game Over: %1 is WINNER!").arg(game_model.gameState === GameModel.Cross ? qsTr("Cross") : qsTr("Nought"))
                        canvas.redraw(indexes[0], indexes[indexes.length - 1])
                    } else {
                        infoLabel.text = qsTr("Dead heat")
                    }
                }
            }
        }
    }
}

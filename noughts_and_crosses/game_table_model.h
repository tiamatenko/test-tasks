#ifndef GAME_TABLE_MODEL_H
#define GAME_TABLE_MODEL_H

#include <QAbstractTableModel>

class GameTableModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_DISABLE_COPY(GameTableModel)
    Q_PROPERTY(int dimension READ dimension NOTIFY dimensionChanged)
    Q_PROPERTY(GameState gameState READ gameState NOTIFY gameStateChanged)
public:
    enum GameDataRoles { CellValueRole = Qt::UserRole + 1 };
    enum GameState { None = -1, Nought = 0, Cross = 1 };
    Q_ENUM(GameState)

    GameTableModel(QObject *parent = nullptr);

    inline int dimension() const { return m_dimension; }
    inline GameState gameState() const { return m_gameState; }
    bool checkIndex(const QModelIndex &index) const;

    // QAbstractItemModel interface
    QHash<int, QByteArray> roleNames() const override { return m_roleNames; }
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

public slots:
    void startGame(int newDimension);
    void nextMove(const QModelIndex &index);

signals:
    void dimensionChanged(int dimension);
    void gameStateChanged(GameState state);
    void gameOver(const QVariantList &points);

private:
    bool checkGameOver(int row, int column);
    void switchState();

private:
    QHash<int, QByteArray> m_roleNames;
    int m_dimension;
    GameState m_gameState;
    QVector< QVector<int> > m_matrix;
};

#endif // GAME_TABLE_MODEL_H

#ifndef GAME_FLAT_PROXY_MODEL_H
#define GAME_FLAT_PROXY_MODEL_H

#include <QAbstractProxyModel>

class GameFlatProxyModel : public QAbstractProxyModel
{
    Q_OBJECT
    Q_DISABLE_COPY(GameFlatProxyModel)
public:
    GameFlatProxyModel(QObject *parent = nullptr);

public:
    // QAbstractItemModel interface
    QModelIndex index(int row, int column = 0, const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;

    // QAbstractProxyModel interface
    QModelIndex mapToSource(const QModelIndex &proxyIndex) const override;
    QModelIndex mapFromSource(const QModelIndex &sourceIndex) const override;

public slots:
    void nextMove(int row);
    void handleGameOver(const QVariantList &points);

private slots:
    void handleSourceModelReset();
    void handleSourceDataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight, const QVector<int> &roles);

signals:
    void gameOver(const QVariantList &indexes);
    void nextMoveDone(const QModelIndex &sourceIndex);
};

#endif // GAME_FLAT_PROXY_MODEL_H

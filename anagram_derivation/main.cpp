#include <QCoreApplication>
#include <QFile>
#include <QCommandLineParser>
#include <QDebug>

#include <iostream>

#include "anagram_generator.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    app.setApplicationName("Anagram derivation");
    app.setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("This program finds the longest anagram derivation from a specific 3-letter word\n"
                                     "(one word provided by user) in a list of words where every derived word also exists in the list of words.");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption firstWordOption({"w", "word"},
            QObject::tr("The first 3-letter <word> of derivation."),
            QObject::tr("word"));
    parser.addOption(firstWordOption);

    QCommandLineOption inputFileOption({"i", "input-file"},
            QObject::tr("<file> of dictionary (optionally)."),
            QObject::tr("file"));
    parser.addOption(inputFileOption);

    QCommandLineOption printDictOption({"p", "print-dict"}, QObject::tr("Print content of dictionary (optionally)"));
    parser.addOption(printDictOption);

    parser.process(app);

    const QString word = parser.value(firstWordOption);
    if (word.isEmpty()) {
        std::cerr << "The first derivation word is missed" << std::endl;
        return 1;
    }

    if (word.length() != 3) {
        std::cerr << "The first derivation word must consist of 3 letter" << std::endl;
        return 1;
    }

    QFile file;
    QString inputFilePath = parser.value(inputFileOption);
    if (!inputFilePath.isEmpty()) {
        file.setFileName(inputFilePath);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            std::cerr << qPrintable(inputFilePath) << ": No such file" << std::endl;
            return 1;
        }
    }

    QStringList words;
    if (file.isOpen()) {
        QTextStream textStream(&file);
        while (!textStream.atEnd()) {
            QString txt;
            textStream >> txt;
            if (!txt.isEmpty())
                words << txt;
        }
    } else {
        words = QStringList({ "ail", "tennis", "nails", "desk", "aliens", "table", "engine", "sail" });
    }

    if (parser.isSet(printDictOption)) {
        qInfo() << "Content of the source dictionary:";
        qInfo() << words;
    }

    AnagramGenerator ag(words);
    qInfo() << "Result anagram:";
    qInfo() << ag.generateAnagram(word);

    return 0;
}

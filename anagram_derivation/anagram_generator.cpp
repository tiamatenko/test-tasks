#include "anagram_generator.h"

AnagramGenerator::AnagramGenerator()
{

}

AnagramGenerator::AnagramGenerator(const QStringList &dict)
{
    setDict(dict);
}

QStringList AnagramGenerator::dict() const
{
    return m_dict;
}

void AnagramGenerator::setDict(const QStringList &dict)
{
    m_dict = dict;

    std::sort(m_dict.begin(), m_dict.end(), [=] (const QString &s1, const QString &s2) { // sort words by their lengths to speed up searching
        return s1.length() < s2.length();
    });
}

QStringList AnagramGenerator::generateAnagram(const QString &firstWord) const
{
    return findAnagram(firstWord, m_dict); // recursive search
}

QStringList AnagramGenerator::findAnagram(const QString &baseWord, const QStringList &dict) const
{
    const int nextLength = baseWord.length() + 1; // length of next word of anagram

    QStringList resultAnagram, possibleDerivedWords;
    QStringList remainDict = dict; // copy dict to make it mutable

    auto it = remainDict.begin();
    while (it != remainDict.end()) {
        const QString &word = *it;

        if (word.length() > nextLength) // break search, all remain words are longer because dict is sorted by word length
            break;

        if (word.length() == nextLength)
            possibleDerivedWords << word; // gather possible derived words

        it = remainDict.erase(it); // remove words with word.length() <= nextLength to reduce remainDict and optimize next searching
    }

    for (const QString &word : possibleDerivedWords) { // search derived words
        bool isDerived = true;
        for (int i = 0; i < baseWord.length(); ++i) {  // check whether the word contains every letter of the base word
            QChar c = baseWord.at(i);
            if (!word.contains(c, Qt::CaseInsensitive)) {
                isDerived = false;
                break;
            }
        }

        if (isDerived) {
            QStringList anagram = findAnagram(word, remainDict); // if word is derived then make recursive call to find next derived words
            if (anagram.count() > resultAnagram.count())         // keep the longest deriviation
                resultAnagram = anagram;
        }
    }

    resultAnagram.prepend(baseWord);

    return resultAnagram;
}

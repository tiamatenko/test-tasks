#ifndef ANAGRAM_GENERATOR_H
#define ANAGRAM_GENERATOR_H

#include <QStringList>

class AnagramGenerator
{
public:
    explicit AnagramGenerator();
    explicit AnagramGenerator(const QStringList &dict);

    /*!
     * \brief Returns list of words, which are used for generation of anagrams.
     * \return
     */
    QStringList dict() const;
    /*!
     * \brief Sets list of words, which are used for generation of anagrams.
     * \param dict
     */
    void setDict(const QStringList &dict);

    /*!
     * \brief Generates an anagram. Dictionary of words should be set before calling this method.
     * \param The first word of anagram
     * \return Words of an anagram
     */
    QStringList generateAnagram(const QString &firstWord) const;

private:
    QStringList findAnagram(const QString &baseWord, const QStringList &dict) const;

private:
    QStringList m_dict;
};

#endif // ANAGRAM_GENERATOR_H

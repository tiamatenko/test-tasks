#include <QtTest>

#include "anagram_generator.h"

#include <QObject>
#include <QDebug>

class tst_AnagramGenerator : public QObject
{
    Q_OBJECT
public:
    tst_AnagramGenerator() {}

private slots:
    void initTestCase();
    void generatorTest();
    void cleanupTestCase();

private:
    AnagramGenerator m_generator;
};


void tst_AnagramGenerator::initTestCase()
{
    QFile file(QStringLiteral("SampleDict.txt"));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QFAIL(qPrintable(file.fileName() + " : No such file"));
        return;
    }

    QStringList words;
    QTextStream textStream(&file);
    while (!textStream.atEnd()) {
        QString txt;
        textStream >> txt;
        if (!txt.isEmpty())
            words << txt;
    }

    m_generator.setDict(words);
}

void tst_AnagramGenerator::generatorTest()
{
    QCOMPARE(m_generator.generateAnagram(QStringLiteral("ail")), QStringList({ "ail", "sail", "nails", "aliens" }));
}

void tst_AnagramGenerator::cleanupTestCase()
{
}


QTEST_MAIN(tst_AnagramGenerator)
#include "tst_anagram_derivation.moc"

#include "round_table_model.h"

#include <QSet>
#include <QPixmap>
#include <QBrush>

#include "global.h"
#include "philosopher.h"
#include "fork.h"

RoundTableModel::RoundTableModel(QObject *parent)
    : QAbstractListModel(parent)
{
    qRegisterMetaType<Philosopher::State>();
    qRegisterMetaType<QSharedPointer<Fork>>();
}

RoundTableModel::~RoundTableModel()
{
    qDebug() << Q_FUNC_INFO;
    for (Philosopher *philosopher : m_philosophers) {
        philosopher->exit();
        philosopher->wait();
    }
}

int RoundTableModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_philosophers.count();
}

QVariant RoundTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= rowCount())
        return QVariant();

    if (role == Qt::EditRole)
        role = Qt::DisplayRole;

    Philosopher *philosopher = m_philosophers.at(index.row());

    static QPixmap px(QStringLiteral(":/philosopher.png"));

    switch (role) {
    case Qt::DisplayRole:
        return philosopher->name();
    case Qt::DecorationRole:
        return px;
    case Qt::BackgroundRole:
        if (philosopher->state() == Philosopher::State::Thinking)
            return QVariant::fromValue(QBrush(Qt::yellow));
        if (philosopher->state() == Philosopher::State::Eating)
            return QVariant::fromValue(QBrush(Qt::green));
        return QVariant::fromValue(QBrush(Qt::red));
    case PhilosopherStateRole:
        if (philosopher->state() == Philosopher::State::Thinking)
            return tr("Thinking");
        if (philosopher->state() == Philosopher::State::Eating)
            return tr("Eating");
        return tr("Starvation");
    }

    return QVariant();
}

void RoundTableModel::addPhilosopher()
{
    int row = rowCount();
    beginInsertRows(QModelIndex(), row, row);

    Philosopher *philosopher = new Philosopher(generatePhilosopherName(), this);

    switch (row) {
    case 0:
        philosopher->setLeftFork(QSharedPointer<Fork>(new Fork));
        philosopher->setRightFork(QSharedPointer<Fork>(new Fork));
        break;
    case 1:
        philosopher->setLeftFork(m_philosophers.first()->rightFork());
        philosopher->setRightFork(m_philosophers.first()->leftFork());
        break;
    default:
        QSharedPointer<Fork> newFork(new Fork);
        philosopher->setRightFork(newFork);
        m_philosophers.first()->setLeftFork(newFork);
        philosopher->setLeftFork(m_philosophers.last()->rightFork());
    }

    m_philosophers << philosopher;

    endInsertRows();

    connect(philosopher, &Philosopher::stateChanged, this, &RoundTableModel::onPhilosopherStateChanged);
    connect(this, &RoundTableModel::thinkingIntervalChanged, &Philosopher::setThinkingInterval);
    connect(this, &RoundTableModel::eatingIntervalChanged, &Philosopher::setEatingInterval);

    philosopher->start();

    emit philosopherAdded(row);
}

void RoundTableModel::removePhilosopher(int row)
{
    if (row < 0 || row >= rowCount())
        return;

    beginRemoveRows(QModelIndex(), row, row);

    Philosopher *philosopher = m_philosophers.takeAt(row);
    if (m_philosophers.size()) {
        Philosopher *prevPhilosopher = (row > 0 ? m_philosophers.at(row - 1) : m_philosophers.last());
        Philosopher *nextPhilosopher = (row < m_philosophers.size() ? m_philosophers.at(row) : m_philosophers.first());
        if (prevPhilosopher != nextPhilosopher)
            nextPhilosopher->setLeftFork(prevPhilosopher->rightFork());
    }
    philosopher->exit();
    philosopher->wait();
    philosopher->deleteLater();

    endRemoveRows();

    emit philosopherRemoved(row);
}

void RoundTableModel::onPhilosopherStateChanged()
{
    Philosopher *philosopher = static_cast<Philosopher*>(sender());
    int row = m_philosophers.indexOf(philosopher);
    if (row < 0)
        return;
    QModelIndex idx = index(row);
    emit dataChanged(idx, idx);
}

QString RoundTableModel::generatePhilosopherName() const
{
    QSet<QString> names;
    for (const Philosopher *philosopher : m_philosophers)
        names << philosopher->name();

    int i = 1;
    QString name;
    do {
        name = QString::fromLatin1("Philosopher %1").arg(i++);
    } while (names.contains(name));

    return name;
}

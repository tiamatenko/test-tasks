#ifndef ROUND_TABLE_MODEL_H
#define ROUND_TABLE_MODEL_H

#include <QAbstractItemModel>

class Philosopher;

class RoundTableModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit RoundTableModel(QObject *parent = Q_NULLPTR);
    ~RoundTableModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

public slots:
    void addPhilosopher();
    void removePhilosopher(int row);

signals:
    void thinkingIntervalChanged(int secs);
    void eatingIntervalChanged(int secs);
    void philosopherAdded(int row);
    void philosopherRemoved(int row);

private slots:
    void onPhilosopherStateChanged();

private:
    QString generatePhilosopherName() const;

private:
    QList<Philosopher*> m_philosophers;
};

#endif // ROUND_TABLE_MODEL_H

#ifndef PHILOSOPHER_H
#define PHILOSOPHER_H

#include <QThread>

#include <QSharedPointer>

class Fork;

class Philosopher : public QThread
{
    Q_OBJECT
public:
    enum class State { Thinking, Eating, Starvation };

    Philosopher(const QString &name, QObject *parent = nullptr);
    ~Philosopher();

    inline QString name() const { return m_name; }
    inline State state() const { return m_state; }

    QSharedPointer<Fork> leftFork() const;
    void setLeftFork(const QSharedPointer<Fork> &leftFork);

    QSharedPointer<Fork> rightFork() const;
    void setRightFork(const QSharedPointer<Fork> &rightFork);

    static void setThinkingInterval(int secs);
    static void setEatingInterval(int secs);

signals:
    void stateChanged();
    void leftForkChanged(const QSharedPointer<Fork> &fork);
    void rightForkChanged(const QSharedPointer<Fork> &fork);


private slots:
    void setState(State state);

private:
    QString m_name;
    State m_state;
    QSharedPointer<Fork> m_leftFork;
    QSharedPointer<Fork> m_rightFork;
};

Q_DECLARE_METATYPE(Philosopher::State)

#endif // PHILOSOPHER_H

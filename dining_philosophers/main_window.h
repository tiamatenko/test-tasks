#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>

class RoundTableModel;
class RoundTableView;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void removePhilosopher();
    void about();

private:
    RoundTableModel *m_model;
    RoundTableView *m_view;
};

#endif // MAIN_WINDOW_H

#ifndef PHILOSOPHER_DELEGATE_H
#define PHILOSOPHER_DELEGATE_H

#include <QStyledItemDelegate>

#include "global.h"

class PhilosopherDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    PhilosopherDelegate(QObject *parent = nullptr);

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // PHILOSOPHER_DELEGATE_H

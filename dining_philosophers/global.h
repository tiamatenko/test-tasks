#ifndef GLOBAL_H
#define GLOBAL_H

#include <Qt>
#include <QDebug>

const int INITIAL_THINKING_EATING_INTERVAL = 1; // seconds
const int INITIAL_PHILOSOPHER_COUNT = 5;

enum { PhilosopherStateRole = Qt::UserRole + 1 };

#endif // GLOBAL_H

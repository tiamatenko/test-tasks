#ifndef FORK_H
#define FORK_H

#include <QSharedPointer>

#include "global.h"

class Fork
{
public:
    Fork();
    ~Fork();

    inline bool isFree() const { return m_threadHandle == nullptr; }
    bool isTaken() const;
    bool isBusy() const;

    void tryTake();
    void release();

private:
    Qt::HANDLE m_threadHandle;
};

Q_DECLARE_METATYPE(QSharedPointer<Fork>)

#endif // FORK_H

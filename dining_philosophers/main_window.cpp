#include "main_window.h"

#include <QMenuBar>
#include <QAction>
#include <QBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QToolButton>
#include <QApplication>
#include <QMessageBox>

#include "global.h"
#include "round_table_model.h"
#include "round_table_view.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    m_model = new RoundTableModel(this);

    m_view = new RoundTableView(this);
    m_view->setStatusTip(tr("Philosophers"));
    m_view->setModel(m_model);

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QAction *exitAction = fileMenu->addAction(QIcon::fromTheme(QStringLiteral("application-exit")), tr("&Quit"));
    exitAction->setShortcut(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Exit the application"));
    connect(exitAction, &QAction::triggered, qApp, &QApplication::exit);

    QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));
    QAction *addAction = editMenu->addAction(QIcon::fromTheme(QStringLiteral("list-add")), tr("&Add Philosopher"));
    addAction->setShortcut(QKeySequence(Qt::Key_Insert));
    addAction->setStatusTip(tr("Add a new philosopher"));
    connect(addAction, &QAction::triggered, m_model, &RoundTableModel::addPhilosopher);

    QAction *removeAction = editMenu->addAction(QIcon::fromTheme(QStringLiteral("list-remove")), tr("&Remove Philosopher"));
    removeAction->setShortcut(QKeySequence::Delete);
    removeAction->setStatusTip(tr("Remove the selected philosopher"));
    connect(removeAction, &QAction::triggered, this, &MainWindow::removePhilosopher);

    m_view->addActions({ addAction, removeAction });

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

    QAction *aboutAct = helpMenu->addAction(QIcon::fromTheme(QStringLiteral("help-about")), tr("&About"));
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, &QAction::triggered, this, &MainWindow::about);

    QAction *aboutQtAct = helpMenu->addAction(style()->standardIcon(QStyle::SP_TitleBarMenuButton), tr("About &Qt"));
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, &QAction::triggered, qApp, &QApplication::aboutQt);

    QHBoxLayout *hBoxLayout = new QHBoxLayout();
    hBoxLayout->setSpacing(20);

    hBoxLayout->addWidget(new QLabel(tr("Thinking interval:"), this));

    QSpinBox *thinkingIntervalSpinBox = new QSpinBox(this);
    thinkingIntervalSpinBox->setSingleStep(1);
    thinkingIntervalSpinBox->setRange(INITIAL_THINKING_EATING_INTERVAL, INITIAL_THINKING_EATING_INTERVAL*10);
    thinkingIntervalSpinBox->setValue(INITIAL_THINKING_EATING_INTERVAL);
    thinkingIntervalSpinBox->setSuffix(tr(" sec(s)"));
    hBoxLayout->addWidget(thinkingIntervalSpinBox);

    hBoxLayout->addWidget(new QLabel(tr("Eating interval:"), this));

    QSpinBox *eatingIntervalSpinBox = new QSpinBox(this);
    eatingIntervalSpinBox->setSingleStep(1);
    eatingIntervalSpinBox->setRange(INITIAL_THINKING_EATING_INTERVAL, INITIAL_THINKING_EATING_INTERVAL*10);
    eatingIntervalSpinBox->setValue(INITIAL_THINKING_EATING_INTERVAL);
    eatingIntervalSpinBox->setSuffix(thinkingIntervalSpinBox->suffix());
    hBoxLayout->addWidget(eatingIntervalSpinBox);

    hBoxLayout->addStretch();

    QToolButton *addButton = new QToolButton(this);
    addButton->setDefaultAction(addAction);
    addButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    addButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    hBoxLayout->addWidget(addButton);

    QWidget *bottomWidget = new QWidget(this);
    bottomWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    bottomWidget->setLayout(hBoxLayout);

    QVBoxLayout *vBoxLayout = new QVBoxLayout();
    vBoxLayout->addWidget(m_view);
    vBoxLayout->addWidget(bottomWidget);

    QWidget *w = new QWidget(this);
    w->setLayout(vBoxLayout);

    setCentralWidget(w);

    m_view->setFocus();

    void (QSpinBox::*valueChangedFunc)(int);
    valueChangedFunc = &QSpinBox::valueChanged;
    connect(thinkingIntervalSpinBox, valueChangedFunc, m_model, &RoundTableModel::thinkingIntervalChanged);
    connect(eatingIntervalSpinBox, valueChangedFunc, m_model, &RoundTableModel::eatingIntervalChanged);
    connect(m_model, &RoundTableModel::philosopherAdded, [&] (int row) {
        QModelIndex index = m_view->model()->index(row, 0);
        m_view->setCurrentIndex(index);
    });

    for (int i = 0; i < INITIAL_PHILOSOPHER_COUNT; ++i)
        m_model->addPhilosopher();
}

MainWindow::~MainWindow()
{

}

void MainWindow::removePhilosopher()
{
    QModelIndex index = m_view->currentIndex();
    if (index.isValid())
        m_model->removePhilosopher(index.row());
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Dining Philosophers"),
            tr("Implementation of dining philosophers problem in Qt:\n"
               "N philosophers sit at a round table with bowls.\n"
               "Forks are placed between each pair of adjacent philosophers.\n"
               "Each philosopher must alternately think and eat.\n"
               "However, a philosopher can only eat when he has both\nleft and right forks."));
}

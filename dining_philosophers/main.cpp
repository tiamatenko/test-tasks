#include "main_window.h"

#include <QApplication>
#include <QStyle>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName(QStringLiteral("Dining Philosophers"));
    a.setApplicationVersion(QStringLiteral("1.0"));
    a.setWindowIcon(a.style()->standardIcon(QStyle::SP_TitleBarMenuButton));

    MainWindow w;
    w.setMinimumSize(800, 600);
    w.showMaximized();

    return a.exec();
}

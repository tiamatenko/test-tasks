#ifndef ROUND_TABLE_VIEW_H
#define ROUND_TABLE_VIEW_H

#include <QListView>

class RoundTableView : public QListView
{
    Q_OBJECT
public:
    RoundTableView(QWidget *parent = nullptr);
};

#endif // ROUND_TABLE_VIEW_H

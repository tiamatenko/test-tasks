#include "round_table_view.h"

#include <QMenu>

#include "philosopher_delegate.h"

RoundTableView::RoundTableView(QWidget *parent)
    : QListView(parent)
{
    setItemDelegate(new PhilosopherDelegate(this));
    
    setSelectionMode(QAbstractItemView::SingleSelection);
    setViewMode(QListView::IconMode);
    setUniformItemSizes(true);
    setResizeMode(QListView::Adjust);
    setSpacing(10);
    setContextMenuPolicy(Qt::ActionsContextMenu);
}

#include "philosopher_delegate.h"

#include <QPainter>

const int SPACING = 5;

PhilosopherDelegate::PhilosopherDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
}

QSize PhilosopherDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QSize sz = QStyledItemDelegate::sizeHint(option, index);
    sz.setWidth(sz.width() + 2*SPACING);
    sz.setHeight(sz.height() + option.fontMetrics.lineSpacing() + 4*SPACING);
    return sz;
}

void PhilosopherDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QRect rect = option.rect.adjusted(SPACING, SPACING, -SPACING, -SPACING);

    painter->fillRect(rect, index.data(Qt::BackgroundRole).value<QBrush>());

    if (option.state & QStyle::State_Selected) {
        painter->save();
        QPen pen(option.palette.highlight(), SPACING);
        painter->setPen(pen);
        painter->drawRect(rect);
        painter->restore();
    }

    QPixmap px = index.data(Qt::DecorationRole).value<QPixmap>();
    painter->drawPixmap(rect.x() + (rect.width() - px.width()) / 2, rect.y(), px);

    rect.setTop(rect.top() + px.height() + SPACING);
    rect.setBottom(rect.top() + option.fontMetrics.lineSpacing());
    QString name = index.data().toString();
    painter->drawText(rect, option.displayAlignment, name);

    rect.moveTop(rect.bottom() + SPACING);
    QString state = index.data(PhilosopherStateRole).toString();
    painter->drawText(rect, option.displayAlignment, state);
}

#include "philosopher.h"

#include <QTimer>
#include <QElapsedTimer>
#include <QMutex>
#include <QWaitCondition>

#include "fork.h"

class Worker : public QObject
{
    Q_OBJECT

public:
    Worker(QObject *parent = nullptr)
        : QObject(parent)
        , m_timer(this)
    {
        m_timer.setSingleShot(true);

        connect(&m_timer, &QTimer::timeout, [&] {
            if (!m_leftFork || !m_rightFork)
                return;

            if (releaseForks())
                return;

            m_mutex.lock();

            while (m_leftFork->isBusy() || m_rightFork->isBusy()) {
                if (!m_starvationTimer.isValid())
                    m_starvationTimer.start();

                qint64 timerValue = m_starvationTimer.elapsed();
                if (timerValue > m_maxStarvation.second) {                               // only the hungriest philosopher can try to take one of two forks
                    m_maxStarvation = qMakePair(QThread::currentThreadId(), timerValue); // save max starvation data
                    m_leftFork->tryTake();
                    m_rightFork->tryTake();
                }

                if (m_starvationTimer.hasExpired(thinkingInterval()))                    // starvation starts when 2 * thinkingInterval() after eating
                    emit stateChanged(Philosopher::State::Starvation);

                m_waitCondition.wait(&m_mutex);
            }

            m_leftFork->tryTake();
            m_rightFork->tryTake();

            clearMaxStarvationData();

            m_mutex.unlock();

            m_starvationTimer.invalidate();
            m_timer.start(eatingInterval());

            emit stateChanged(Philosopher::State::Eating);
        });
    }

    ~Worker()
    {
        releaseForks();
        m_timer.stop();
    }

    inline QSharedPointer<Fork> leftFork() const { return m_leftFork; }
    inline QSharedPointer<Fork> rightFork() const { return m_rightFork; }

    static inline int thinkingInterval() { return m_thinkingInterval; }
    static inline void setThinkingInterval(int secs) { m_thinkingInterval = secs * 1000; }

    static inline int eatingInterval() { return m_eatingInterval; }
    static inline void setEatingInterval(int secs) { m_eatingInterval = secs * 1000; }

    static inline void clearMaxStarvationData()
    {
        if (QThread::currentThreadId() == m_maxStarvation.first)
            m_maxStarvation = qMakePair(nullptr, 0);
    }

public slots:
    void setLeftFork(const QSharedPointer<Fork> &leftFork)
    {
        releaseForks();

        m_leftFork = leftFork;

        m_starvationTimer.invalidate();
        if (m_leftFork && m_rightFork)
            m_timer.start(thinkingInterval());
        else
            m_timer.stop();
    }

    void setRightFork(const QSharedPointer<Fork> &rightFork)
    {
        releaseForks();

        m_rightFork = rightFork;

        m_starvationTimer.invalidate();
        if (m_leftFork && m_rightFork)
            m_timer.start(thinkingInterval());
        else
            m_timer.stop();
    }

signals:
    void stateChanged(Philosopher::State state);

private:
    bool releaseForks()
    {
        if (m_leftFork && !m_leftFork->isTaken() && m_rightFork && !m_rightFork->isTaken())
            return false;

        m_mutex.lock();

        if (m_leftFork)
            m_leftFork->release();
        if (m_rightFork)
            m_rightFork->release();

        clearMaxStarvationData();

        m_waitCondition.wakeAll();
        m_mutex.unlock();

        m_timer.start(thinkingInterval());
        emit stateChanged(Philosopher::State::Thinking);
        return true;
    }

private:
    // Local thread data
    QTimer m_timer;
    QElapsedTimer m_starvationTimer;

    // Shared data between 2 threads
    QSharedPointer<Fork> m_leftFork;
    QSharedPointer<Fork> m_rightFork;

    // Static shared data between all threads
    static int m_thinkingInterval;
    static int m_eatingInterval;
    static QPair<Qt::HANDLE, qint64> m_maxStarvation; // first: thread's handle, second: maximum

    // Synchronization threads data
    static QMutex m_mutex;
    static QWaitCondition m_waitCondition;
};

int Worker::m_thinkingInterval(INITIAL_THINKING_EATING_INTERVAL*1000);
int Worker::m_eatingInterval(INITIAL_THINKING_EATING_INTERVAL*1000);
QPair<Qt::HANDLE, qint64> Worker::m_maxStarvation(nullptr, 0);

QMutex Worker::m_mutex;
QWaitCondition Worker::m_waitCondition;



Philosopher::Philosopher(const QString &name, QObject *parent)
    : QThread(parent)
    , m_name(name)
    , m_state(State::Thinking)
{
    Worker *worker = new Worker();
    worker->moveToThread(this);
    connect(worker, &Worker::stateChanged, this, &Philosopher::setState);
    connect(this, &Philosopher::leftForkChanged, worker, &Worker::setLeftFork);
    connect(this, &Philosopher::rightForkChanged, worker, &Worker::setRightFork);
    connect(this, &Philosopher::finished, worker, &Worker::deleteLater);
}

Philosopher::~Philosopher()
{
    qDebug() << Q_FUNC_INFO;
}

QSharedPointer<Fork> Philosopher::leftFork() const
{
    return m_leftFork;
}

void Philosopher::setLeftFork(const QSharedPointer<Fork> &leftFork)
{
    m_leftFork = leftFork;
    emit leftForkChanged(leftFork);
}

QSharedPointer<Fork> Philosopher::rightFork() const
{
    return m_rightFork;
}

void Philosopher::setRightFork(const QSharedPointer<Fork> &rightFork)
{
    m_rightFork = rightFork;
    emit rightForkChanged(rightFork);
}

void Philosopher::setThinkingInterval(int secs)
{
    Worker::setThinkingInterval(secs);
}

void Philosopher::setEatingInterval(int secs)
{
    Worker::setEatingInterval(secs);
}

void Philosopher::setState(Philosopher::State state)
{
    if (m_state != state) {
        m_state = state;
        emit stateChanged();
    }
}

#include "philosopher.moc"

#include "fork.h"

#include <QThread>

Fork::Fork() : m_threadHandle(nullptr)
{
}

Fork::~Fork()
{
    qDebug() << Q_FUNC_INFO;
}

bool Fork::isTaken() const
{
    return m_threadHandle == QThread::currentThreadId();
}

bool Fork::isBusy() const
{
    return !isFree() && !isTaken();
}

void Fork::tryTake()
{
    if (isFree())
        m_threadHandle = QThread::currentThreadId();
}

void Fork::release()
{
    if (isTaken())
        m_threadHandle = nullptr;
}
